﻿namespace PerceptronAlgorithm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.getFunctionsButton = new System.Windows.Forms.Button();
            this.functionsTextBox = new System.Windows.Forms.RichTextBox();
            this.classesLabel = new System.Windows.Forms.Label();
            this.vectorsLabel = new System.Windows.Forms.Label();
            this.attributesLabel = new System.Windows.Forms.Label();
            this.classesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.attributesNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.vectorsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.functionsLabel = new System.Windows.Forms.Label();
            this.vectorDataGrid = new System.Windows.Forms.DataGridView();
            this.enterVectorLabel = new System.Windows.Forms.Label();
            this.classifyButton = new System.Windows.Forms.Button();
            this.classLabel = new System.Windows.Forms.Label();
            this.warningLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.classesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributesNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vectorsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vectorDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // getFunctionsButton
            // 
            this.getFunctionsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.getFunctionsButton.Location = new System.Drawing.Point(12, 106);
            this.getFunctionsButton.Name = "getFunctionsButton";
            this.getFunctionsButton.Size = new System.Drawing.Size(315, 24);
            this.getFunctionsButton.TabIndex = 0;
            this.getFunctionsButton.Text = "Рассчитать";
            this.getFunctionsButton.UseVisualStyleBackColor = true;
            this.getFunctionsButton.Click += new System.EventHandler(this.getFunctionsButton_Click);
            // 
            // functionsTextBox
            // 
            this.functionsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.functionsTextBox.Location = new System.Drawing.Point(12, 162);
            this.functionsTextBox.Name = "functionsTextBox";
            this.functionsTextBox.Size = new System.Drawing.Size(315, 126);
            this.functionsTextBox.TabIndex = 1;
            this.functionsTextBox.Text = "";
            // 
            // classesLabel
            // 
            this.classesLabel.AutoSize = true;
            this.classesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.classesLabel.Location = new System.Drawing.Point(12, 19);
            this.classesLabel.Name = "classesLabel";
            this.classesLabel.Size = new System.Drawing.Size(145, 16);
            this.classesLabel.TabIndex = 2;
            this.classesLabel.Text = "Количество классов:";
            // 
            // vectorsLabel
            // 
            this.vectorsLabel.AutoSize = true;
            this.vectorsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vectorsLabel.Location = new System.Drawing.Point(12, 47);
            this.vectorsLabel.Name = "vectorsLabel";
            this.vectorsLabel.Size = new System.Drawing.Size(213, 16);
            this.vectorsLabel.TabIndex = 3;
            this.vectorsLabel.Text = "Количество векторов в классе:";
            // 
            // attributesLabel
            // 
            this.attributesLabel.AutoSize = true;
            this.attributesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.attributesLabel.Location = new System.Drawing.Point(12, 76);
            this.attributesLabel.Name = "attributesLabel";
            this.attributesLabel.Size = new System.Drawing.Size(231, 16);
            this.attributesLabel.TabIndex = 4;
            this.attributesLabel.Text = "Количество признаков у вектора:";
            // 
            // classesNumericUpDown
            // 
            this.classesNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.classesNumericUpDown.Location = new System.Drawing.Point(249, 17);
            this.classesNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.classesNumericUpDown.Name = "classesNumericUpDown";
            this.classesNumericUpDown.Size = new System.Drawing.Size(78, 22);
            this.classesNumericUpDown.TabIndex = 5;
            this.classesNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // attributesNumericUpDown
            // 
            this.attributesNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.attributesNumericUpDown.Location = new System.Drawing.Point(249, 74);
            this.attributesNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.attributesNumericUpDown.Name = "attributesNumericUpDown";
            this.attributesNumericUpDown.Size = new System.Drawing.Size(78, 22);
            this.attributesNumericUpDown.TabIndex = 6;
            this.attributesNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // vectorsNumericUpDown
            // 
            this.vectorsNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vectorsNumericUpDown.Location = new System.Drawing.Point(249, 45);
            this.vectorsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.vectorsNumericUpDown.Name = "vectorsNumericUpDown";
            this.vectorsNumericUpDown.Size = new System.Drawing.Size(78, 22);
            this.vectorsNumericUpDown.TabIndex = 7;
            this.vectorsNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // functionsLabel
            // 
            this.functionsLabel.AutoSize = true;
            this.functionsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.functionsLabel.Location = new System.Drawing.Point(12, 143);
            this.functionsLabel.Name = "functionsLabel";
            this.functionsLabel.Size = new System.Drawing.Size(155, 16);
            this.functionsLabel.TabIndex = 8;
            this.functionsLabel.Text = "Полученные функции:";
            // 
            // vectorDataGrid
            // 
            this.vectorDataGrid.AllowUserToAddRows = false;
            this.vectorDataGrid.AllowUserToDeleteRows = false;
            this.vectorDataGrid.AllowUserToResizeColumns = false;
            this.vectorDataGrid.AllowUserToResizeRows = false;
            this.vectorDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.vectorDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.vectorDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vectorDataGrid.ColumnHeadersVisible = false;
            this.vectorDataGrid.Location = new System.Drawing.Point(360, 47);
            this.vectorDataGrid.Name = "vectorDataGrid";
            this.vectorDataGrid.RowHeadersVisible = false;
            this.vectorDataGrid.Size = new System.Drawing.Size(245, 33);
            this.vectorDataGrid.TabIndex = 9;
            // 
            // enterVectorLabel
            // 
            this.enterVectorLabel.AutoSize = true;
            this.enterVectorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.enterVectorLabel.Location = new System.Drawing.Point(357, 19);
            this.enterVectorLabel.Name = "enterVectorLabel";
            this.enterVectorLabel.Size = new System.Drawing.Size(248, 16);
            this.enterVectorLabel.TabIndex = 10;
            this.enterVectorLabel.Text = "Введите вектор для классификации:";
            // 
            // classifyButton
            // 
            this.classifyButton.Enabled = false;
            this.classifyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.classifyButton.Location = new System.Drawing.Point(360, 107);
            this.classifyButton.Name = "classifyButton";
            this.classifyButton.Size = new System.Drawing.Size(245, 23);
            this.classifyButton.TabIndex = 11;
            this.classifyButton.Text = "Классифицировать";
            this.classifyButton.UseVisualStyleBackColor = true;
            this.classifyButton.Click += new System.EventHandler(this.classifyButton_Click);
            // 
            // classLabel
            // 
            this.classLabel.AutoSize = true;
            this.classLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.classLabel.ForeColor = System.Drawing.Color.Green;
            this.classLabel.Location = new System.Drawing.Point(366, 78);
            this.classLabel.Name = "classLabel";
            this.classLabel.Size = new System.Drawing.Size(0, 16);
            this.classLabel.TabIndex = 12;
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.warningLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.warningLabel.Location = new System.Drawing.Point(12, 301);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(0, 16);
            this.warningLabel.TabIndex = 13;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 342);
            this.Controls.Add(this.warningLabel);
            this.Controls.Add(this.classLabel);
            this.Controls.Add(this.classifyButton);
            this.Controls.Add(this.enterVectorLabel);
            this.Controls.Add(this.vectorDataGrid);
            this.Controls.Add(this.functionsLabel);
            this.Controls.Add(this.vectorsNumericUpDown);
            this.Controls.Add(this.attributesNumericUpDown);
            this.Controls.Add(this.classesNumericUpDown);
            this.Controls.Add(this.attributesLabel);
            this.Controls.Add(this.vectorsLabel);
            this.Controls.Add(this.classesLabel);
            this.Controls.Add(this.functionsTextBox);
            this.Controls.Add(this.getFunctionsButton);
            this.Name = "MainForm";
            this.Text = "Метод Персептрона";
            ((System.ComponentModel.ISupportInitialize)(this.classesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.attributesNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vectorsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vectorDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button getFunctionsButton;
        private System.Windows.Forms.RichTextBox functionsTextBox;
        private System.Windows.Forms.Label classesLabel;
        private System.Windows.Forms.Label vectorsLabel;
        private System.Windows.Forms.Label attributesLabel;
        private System.Windows.Forms.NumericUpDown classesNumericUpDown;
        private System.Windows.Forms.NumericUpDown attributesNumericUpDown;
        private System.Windows.Forms.NumericUpDown vectorsNumericUpDown;
        private System.Windows.Forms.Label functionsLabel;
        private System.Windows.Forms.DataGridView vectorDataGrid;
        private System.Windows.Forms.Label enterVectorLabel;
        private System.Windows.Forms.Button classifyButton;
        private System.Windows.Forms.Label classLabel;
        private System.Windows.Forms.Label warningLabel;
    }
}

