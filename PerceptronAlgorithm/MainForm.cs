﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PerceptronAlgorithm.Algorithm;
using PerceptronAlgorithm.FormWork;

namespace PerceptronAlgorithm
{
    public partial class MainForm : Form
    {
        private IFormWork _formWork;

        public MainForm()
        {
            InitializeComponent();
        }

        private void getFunctionsButton_Click(object sender, EventArgs e)
        {
            _formWork = new FormWork.FormWork((int)classesNumericUpDown.Value, (int)vectorsNumericUpDown.Value, (int)attributesNumericUpDown.Value + 1);

            warningLabel.Text = _formWork.Work() ? string.Empty : "Итерационный процесс не сошелся";

            functionsTextBox.Text = _formWork.GetFunctions();

            CustomizeVectorDataGrid((int)attributesNumericUpDown.Value);

            classifyButton.Enabled = true;
        }

        private void CustomizeVectorDataGrid(int columnsCount)
        {
            vectorDataGrid.ColumnCount = columnsCount;
            vectorDataGrid.RowCount = 1;
            vectorDataGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void classifyButton_Click(object sender, EventArgs e)
        {
            var attributes = new int[vectorDataGrid.ColumnCount + 1];

            for (var i = 0; i < vectorDataGrid.ColumnCount; i++)
            {
                attributes[i] = int.Parse((string)vectorDataGrid[i, 0].Value);
            }

            classLabel.Text = _formWork.Classify(attributes);
        }
    }
}
