﻿using System.Text;
using PerceptronAlgorithm.Algorithm;

namespace PerceptronAlgorithm.FormWork
{
    public class FormWork : IFormWork
    {
        private readonly Perceptron _perceptron;

        public FormWork(int classesNumber, int vectorsNumber, int attributesNumber)
        {
            _perceptron = new Perceptron(classesNumber, vectorsNumber, attributesNumber);
        }

        public bool Work()
        {
            return _perceptron.Work();
        }

        public string GetFunctions()
        {
            var functions = new StringBuilder();

            for (var i = 0; i < _perceptron.Functions.Length; i++)
            {
                functions.Append($"d{i + 1}(x) = {_perceptron.Functions[i]}\n");
            }

            return functions.ToString();
        }

        public string Classify(int[] attributes)
        {
            return $"Вектор относится к {_perceptron.Classify(attributes) + 1} классу.";
        }
    }
}
