﻿using System.Runtime.InteropServices;
using PerceptronAlgorithm.Algorithm;

namespace PerceptronAlgorithm.FormWork
{
    public interface IFormWork
    {
        bool Work();

        string GetFunctions();

        string Classify(int[] attributes);
    }
}