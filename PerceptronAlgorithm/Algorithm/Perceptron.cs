﻿using System;
using System.Collections.Generic;

namespace PerceptronAlgorithm.Algorithm
{
    public class Perceptron
    {
        private readonly int _classesNumber;

        private readonly int _vectorsNumber;

        public Vector[,] Vectors { get; private set; }

        public Function[] Functions { get; private set; }

        public Perceptron(int classesNumber, int vectorsNumber, int attributesNumber)
        {
            _classesNumber = classesNumber;
            _vectorsNumber = vectorsNumber;
            CreateVectors(attributesNumber);
            CreateFunctions(attributesNumber);
        }

        private void CreateVectors(int attributesNumber)
        {
            Vectors = new Vector[_classesNumber,_vectorsNumber];
            var random = new Random();

            for (var i = 0; i < _classesNumber; i++)
            {
                for (var j = 0; j < _vectorsNumber; j++)
                {
                    Vectors[i, j] = new Vector(attributesNumber,random);
                }
            }
        }

        private void CreateFunctions(int attributesNumber)
        {
            Functions = new Function[_classesNumber];

            for (var i = 0; i < _classesNumber; i++)
            {
                Functions[i] = new Function(attributesNumber);
            }
        }

        public bool Work()
        {
            var iterationsNumber = 0;

            while (Iterate(false) && iterationsNumber++ < 1000) ;

            return iterationsNumber <= 1000;
        }

        public int Classify(int[] attributes)
        {
            var maxValue = 0;
            var maxClass = 0;

            for (var i = 0; i < _classesNumber; i++)
            {
                var value = Functions[i].Multiply(attributes);

                if (maxValue < value)
                {
                    maxValue = value;
                    maxClass = i;
                }
            }

            return maxClass;
        }

        private bool Iterate(bool isNextIteration)
        {
            for (var i = 0; i < _classesNumber; i++)
            {
                for (var j = 0; j < _vectorsNumber; j++)
                {
                    if (Multiply(Vectors[i, j], i)) isNextIteration = true;
                }
            }

            return isNextIteration;
        }

        private bool Multiply(Vector vector, int mainClass)
        {
            var functionValues = GetAllValues(vector);

            if (!CheckChangeCoefficients(functionValues, mainClass)) return false;

            ChangeCoefficients(functionValues, vector, mainClass);

            return true;
        }

        private int[] GetAllValues(Vector vector)
        {
            var functionValues = new int[_classesNumber];

            for (var i = 0; i < _classesNumber; i++)
            {
                functionValues[i] = Functions[i].Multiply(vector.Attributes);
            }

            return functionValues;
        }

        private bool CheckChangeCoefficients(IReadOnlyList<int> functionValues, int mainClass)
        {
            for (var i = 0; i < functionValues.Count; i++)
            {
                if (i!= mainClass && functionValues[i] >= functionValues[mainClass])
                    return true;
            }

            return false;
        }

        private void ChangeCoefficients(int[] functionValues, Vector vector, int mainClass)
        {
            for (var i = 0; i < functionValues.Length; i++)
            {
                if (i == mainClass)
                {
                    Functions[i].Add(vector.Attributes);
                }
                else
                {
                    if (functionValues[i] >= functionValues[mainClass])
                        Functions[i].Subtract(vector.Attributes);
                }
            }
        }

    }
}
