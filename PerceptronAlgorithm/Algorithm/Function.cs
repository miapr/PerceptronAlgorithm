﻿using System.Linq;
using System.Text;

namespace PerceptronAlgorithm.Algorithm
{
    public class Function
    {
        public int[] Coefficients { get; }

        public Function(int size)
        {
            Coefficients = new int[size];
        }

        public int Multiply(int[] vectorAttributes)
        {
            return Coefficients.Select((c, i) => vectorAttributes[i] * c).Sum();
        }

        public void Add(int[] vectorAttributes)
        {
            for (var i = 0; i < vectorAttributes.Length; i++)
            {
                Coefficients[i] += vectorAttributes[i];
            }
        }

        public void Subtract(int[] vectorAttributes)
        {
            for (var i = 0; i < vectorAttributes.Length; i++)
            {
                Coefficients[i] -= vectorAttributes[i];
            }
        }

        public override string ToString()
        {
            var function = new StringBuilder();

            for (var i = 0; i < Coefficients.Length; i++)
            {
                var coefficient = i == 0 || Coefficients[i] < 0 ? Coefficients[i].ToString() : $"+{Coefficients[i]}";
                function.Append(i != Coefficients.Length - 1 ? $"{coefficient}*x{i + 1}" : $"{coefficient} ");
            }

            return function.ToString();
        }
    }
}