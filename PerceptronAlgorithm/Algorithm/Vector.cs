﻿using System;
using System.Text;

namespace PerceptronAlgorithm.Algorithm
{
    public class Vector
    {
        public int[] Attributes { get; }

        public Vector(int size, Random random)
        {
            Attributes = new int[size];
            CreateAttributes(size,random);
        }

        public void CreateAttributes(int size, Random random)
        {
            for (var i = 0; i < size; i++)
            {
                var randomValue = random.Next(-10, 10);
                Attributes[i] = randomValue;
            }
        }

        public override string ToString()
        {
            var function = new StringBuilder();

            foreach (var attribute in Attributes)
            {
                function.Append($"{attribute}; ");
            }

            return $"{{ {function} }}";
        }
    }
}